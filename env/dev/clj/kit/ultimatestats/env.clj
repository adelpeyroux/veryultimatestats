(ns kit.ultimatestats.env
  (:require
    [clojure.tools.logging :as log]
    [kit.ultimatestats.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init       (fn []
                 (log/info "\n-=[ultimatestats starting using the development or test profile]=-"))
   :start      (fn []
                 (log/info "\n-=[ultimatestats started successfully using the development or test profile]=-"))
   :stop       (fn []
                 (log/info "\n-=[ultimatestats has shut down successfully]=-"))
   :middleware wrap-dev
   :opts       {:profile       :dev
                :persist-data? true}})
