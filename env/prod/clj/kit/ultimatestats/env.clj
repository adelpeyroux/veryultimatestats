(ns kit.ultimatestats.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init       (fn []
                 (log/info "\n-=[ultimatestats starting]=-"))
   :start      (fn []
                 (log/info "\n-=[ultimatestats started successfully]=-"))
   :stop       (fn []
                 (log/info "\n-=[ultimatestats has shut down successfully]=-"))
   :middleware (fn [handler _] handler)
   :opts       {:profile :prod}})
