(ns kit.ultimatestats.time
  (:require
   [clojure.string :as str]))


(defn get-date [timestamp]
  (let [date-vector (str/split (first (str/split timestamp #" ")) #"-")]
    (into (sorted-map) (map vector [:year :month :day] date-vector))))
