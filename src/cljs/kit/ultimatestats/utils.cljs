(ns kit.ultimatestats.utils)

(defn fmap [fnc dict]
  (into {} (for [[k v] dict] [k (fnc v)])))

(defn histogram [m]
  (map (fn [[k v]] {:value k :count v}) (fmap count (group-by second m ))))

(defn bar-data [m]
  (map (fn [[k v]] {:key k :value v}) (fmap count (group-by identity m ))))

(defn json-to-dict  [json]
  (js->clj (js/JSON.parse json) :keywordize-keys true))

(defn handler [response]
  (.log js/console (str response)))

(defn error-handler [{:keys [status status-text]}]
  (.log js/console
        (str "something bad happened: " status " " status-text)))
