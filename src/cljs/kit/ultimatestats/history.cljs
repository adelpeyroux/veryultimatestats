(ns kit.ultimatestats.history
  (:require
   [react-select :refer [default] :rename {default react-select}]
   [ajax.core :refer [POST]]
   [reagent.core :as r]
   [clojure.string :as str]
   [kit.ultimatestats.characters :refer [characters]]
   [kit.ultimatestats.utils :as utils]
   [kit.ultimatestats.data :as data]))


;; -------------------------
;; Handlers

(defn new-game-handler [response]
  (let [game (utils/json-to-dict response)]
    (data/save-game! game)))

;; -------------------------
;; Requests

(defn send-new-game [game-data on-success]
  (POST "/save-game" {:headers {"x-csrf-token" (.-value (.getElementById js/document "csrf-token"))}
                      :params game-data
                      :format :json
                      :keywords? true
                      :error-handler utils/error-handler
                      :handler #(do (new-game-handler %1) (on-success))
                      :reponse-format "application/json"} game-data))

(defn send-delete-game [game]
  (POST "/delete-game" {:headers {"x-csrf-token" (.-value (.getElementById js/document "csrf-token"))}
                        :params {:id (:id game)}
                        :format :json
                        :keywords? true
                        :error-handler utils/error-handler
                        :handler #(data/delete-game! game)
                        :reponse-format "application/json"} {:id (:id game)}))

;; -------------------------
;; Views


(defn content-box [content]
  [:div.box [:div.multicolortext content]])

(defn char-img [char]
  [:img.character-img {:src (str "/images/chars/" char ".png")}])


(defn select-item [js-item]
  (let [item (js->clj js-item :keywordize-keys true)]
    [:div.nobr
     [char-img (:value item)]
     [:span " ~ " (:label item)]]))

(defn game-box [game]
  [content-box
   [:div.game
    [:p.nobr.nospace (str "#" (:id game) " - "  (:playercount game) " players : ")
     (for [char (:characters game)]
       ^{:key char} (char-img char))
     [:span.right-align
      [:button.ui-button.multicolortext
       {:on-click #(send-delete-game game)}
       "delete"]]]
    [:span.right-align.nospace.timestamp (:timestamp game)]]])

(defn game-input []
  (let [value (r/atom "") select-state (r/atom nil)]
    (fn []
      [content-box [:div.game-input
                    [:> react-select
                     {:is-multi true,
                      :options (clj->js characters),
                      :value @select-state,
                      :className "react-select-container",
                      :formatOptionLabel (fn [x] [(r/as-element [select-item x])]),
                      :on-change #(do (reset! value (str/join "," (map (fn [x] (:value x)) (js->clj % :keywordize-keys true))))
                                      (reset! select-state %1))}]
                    [:button.ui-button.multicolortext
                     {:on-click #(send-new-game
                                  {:playercount (count (str/split @value #",")) :characters @value}
                                  (fn [] (reset! select-state nil)))}
                     "Save"]]])))

(defn game-history [games]
  [:div.game-history
   (for [game games]
     ^{:key (:id game)} [game-box game])])

(defn page []
  [:div
   [game-input]
   [game-history (data/get-games)]])
