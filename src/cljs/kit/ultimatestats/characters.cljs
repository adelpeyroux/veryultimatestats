(ns kit.ultimatestats.characters)


(def characters
  [{:key 0  :value "buddy" :label "Banjo & Kazooie"}
   {:key 1  :value "bayonetta" :label "Bayonetta"}
   {:key 2  :value "koopa" :label "Bowser"}
   {:key 3  :value "koopajr" :label "Bowser Jr."}
   {:key 4  :value "master" :label "Byleth"}
   {:key 5  :value "captain" :label "Captain Falcon"}
   {:key 6  :value "chrom" :label "Chrom"}
   {:key 7  :value "cloud" :label "Cloud"}
   {:key 8  :value "kamui" :label "Corrin"}
   {:key 9  :value "daisy" :label "Daisy"}
   {:key 10 :value "reflet" :label "Daraen"}
   {:key 11 :value "pitb" :label "Dark Pit"}
   {:key 12 :value "samusd" :label "Samus Sombre"}
   {:key 13 :value "diddy" :label "Diddy Kong"}
   {:key 14 :value "donkey" :label "Donkey Kong"}
   {:key 15 :value "mariod" :label "Dr. Mario"}
   {:key 16 :value "duckhunt" :label "Duck Hunt"}
   {:key 17 :value "ptrainer" :label "Entraineur Pokemon"}
   {:key 18 :value "falco" :label "Falco"}
   {:key 19 :value "fox" :label "Fox"}
   {:key 20 :value "ganon" :label "Ganondorf"}
   {:key 21 :value "gekkouga" :label "Amphinobi"}
   {:key 22 :value "brave" :label "Hero"}
   {:key 23 :value "ice_climber" :label "Ice Climbers"}
   {:key 24 :value "ike" :label "Ike"}
   {:key 25 :value "gaogaen" :label "Felinferno"}
   {:key 26 :value "inkling" :label "Inkling"}
   {:key 27 :value "shizue" :label "Marie"}
   {:key 28 :value "purin" :label "Rondoudou"}
   {:key 29 :value "jack" :label "Joker"}
   {:key 30 :value "demon" :label "Kazuya"}
   {:key 31 :value "ken" :label "Ken"}
   {:key 32 :value "dedede" :label "Roi Dadidou"}
   {:key 33 :value "krool" :label "King K. Rool"}
   {:key 34 :value "kirby" :label "Kirby"}
   {:key 35 :value "link" :label "Link"}
   {:key 36 :value "littlemac" :label "Little Mac"}
   {:key 37 :value "lucario" :label "Lucario"}
   {:key 38 :value "lucas" :label "Lucas"}
   {:key 39 :value "lucina" :label "Lucina"}
   {:key 40 :value "luigi" :label "Luigi"}
   {:key 41 :value "mario" :label "Mario"}
   {:key 42 :value "marth" :label "Marth"}
   {:key 43 :value "rockman" :label "Mega Man"}
   {:key 44 :value "metaknight" :label "Meta Knight"}
   {:key 45 :value "mewtwo" :label "Mewtwo"}
   {:key 46 :value "tantan" :label "Min Min"}
   {:key 47 :value "gamewatch" :label "Mr. Game & Watch"}
   {:key 48 :value "ness" :label "Ness"}
   {:key 49 :value "pikmin" :label "Olimar"}
   {:key 50 :value "pacman" :label "Pac-Man"}
   {:key 51 :value "palutena" :label "Palutena"}
   {:key 52 :value "peach" :label "Peach"}
   {:key 53 :value "pichu" :label "Pichu"}
   {:key 54 :value "pikachu" :label "Pikachu"}
   {:key 55 :value "packun" :label "Plante Piranha"}
   {:key 56 :value "pit" :label "Pit"}
   {:key 57 :value "eflame" :label "Pyra/Mythra"}
   {:key 58 :value "richter" :label "Richter"}
   {:key 59 :value "ridley" :label "Ridley"}
   {:key 60 :value "robot" :label "R.O.B."}
   {:key 61 :value "rosetta" :label "Harmonie & Luma"}
   {:key 62 :value "roy" :label "Roy"}
   {:key 63 :value "ryu" :label "Ryu"}
   {:key 64 :value "samus" :label "Samus"}
   {:key 65 :value "edge" :label "Sephiroth"}
   {:key 66 :value "sheik" :label "Sheik"}
   {:key 67 :value "shulk" :label "Shulk"}
   {:key 68 :value "simon" :label "Simon"}
   {:key 69 :value "snake" :label "Snake"}
   {:key 70 :value "sonic" :label "Sonic"}
   {:key 71 :value "trail" :label "Sora"}
   {:key 72 :value "pickel" :label "Steve"}
   {:key 73 :value "dolly" :label "Terry"}
   {:key 74 :value "toonlink" :label "Link Cartoon"}
   {:key 75 :value "murabito" :label "Villageois"}
   {:key 76 :value "wario" :label "Wario"}
   {:key 77 :value "wiifit" :label "Entraineur Wii Fit"}
   {:key 78 :value "wolf" :label "Wolf"}
   {:key 79 :value "yoshi" :label "Yoshi"}
   {:key 80 :value "younglink" :label "Link Enfant"}
   {:key 81 :value "zelda" :label "Zelda"}
   {:key 82 :value "szerosuit" :label "Samus Sans Armure"}])


(defn get-label [value]
  (:label (first (filter #(= (:value %1) value) characters))))
