(ns kit.ultimatestats.stats
  (:require
   [reagent.core :as reagent]
   [recharts]
   [kit.ultimatestats.characters :as chars]
   [kit.ultimatestats.utils :as utils]
   [kit.ultimatestats.data :as data]
   [clojure.string :as str]))


(def XAxis (reagent/adapt-react-class (aget recharts "XAxis")))
(def YAxis (reagent/adapt-react-class (aget recharts "YAxis")))
(def CartesianGrid (reagent/adapt-react-class (aget recharts "CartesianGrid")))
(def Tooltip (reagent/adapt-react-class (aget recharts "Tooltip")))
(def Legend (reagent/adapt-react-class (aget recharts "Legend")))
(def LabelList (reagent/adapt-react-class (aget recharts "LabelList")))
(def Label (reagent/adapt-react-class (aget recharts "Label")))
(def Text (reagent/adapt-react-class (aget recharts "Text")))

(def BarChart (reagent/adapt-react-class (aget recharts "BarChart")))
(def ResponsiveContainer (reagent/adapt-react-class (aget recharts "ResponsiveContainer")))
(def Bar (reagent/adapt-react-class (aget recharts "Bar")))

(def color "#9c498f")

(defn hist-widget [desc label hist-data]
  [:div
   [:h2 desc]
   [BarChart {:data hist-data :width 400 :height 250 :margin {:top 10 :bottom 40 :left 10 :right 10}}
    [XAxis {:dataKey "value"}
     [Label {:value label :position "bottom"}]]
    [CartesianGrid {:strokeDasharray "3 3" :fill "#e6dc55A0"}]
    ;;[Tooltip {:formatter format-tooltip}]
    [Bar {:dataKey "count" :fill color}
     [LabelList {:dataKey "count" :position "inside"}]]]])

(defn char-img [char]
  (.log js/console (str (js->clj char :keywordize-keys true)))
  [:img.character-img {:src (str "/images/chars/" char ".png")}])


(defn bar-chart [desc data]
  [:div
   [:h2 desc]
   [ResponsiveContainer {:width "100%" :height 600}
    [BarChart {:data data :layout "horizontal"}
     [CartesianGrid {:strokeDasharray "3 3"  :fill "#e6dc55A0"}]
     [Bar {:dataKey "value" :fill color}
      [LabelList {:dataKey "label" :position "inside" :angle "90"}]]]]])

(defn get-games-per-session [games]
  (utils/fmap count (group-by #(:timestamp %1) (map data/format-game games))))

(defn get-players-per-game [games]
  (into {} (map (fn [game] [(game :id) (game :playercount)]) games)))

(defn get-all-characters [games]
  (flatten (map #(:characters %1) games)))

(defn game-per-session-histogram [games-data]
  [hist-widget "Games per day :" "# of games" (sort #(< (:value %1) (:value %2)) (utils/histogram (get-games-per-session games-data)))])

(defn player-per-game-histogram [games-data]
  [hist-widget "Players per Game :" "# of players" (sort #(< (:value %1) (:value %2)) (utils/histogram (get-players-per-game games-data)))])

(defn characters-bar-chart [games-data]
  [bar-chart "Characters :" (sort #(> (:value %1) (:value %2))
                                  (map
                                   (fn [entry] {:key (:key entry) :value (:value entry) :label (str/replace (chars/get-label (:key entry)) #" " ".")})
                                   (utils/bar-data (get-all-characters games-data))))])


(defn page []
  [:div.multicolortext
   [:h1 "Funny Stats"]
   [game-per-session-histogram (data/get-games)]
   [player-per-game-histogram (data/get-games)]
   [characters-bar-chart (data/get-games)]])
