(ns kit.ultimatestats.core
  (:require
   [reagent.core :as r]
   [reagent.dom :as d]
   [kit.ultimatestats.history :as hist]
   [kit.ultimatestats.stats :as stats]
   [kit.ultimatestats.about :as about]
   [kit.ultimatestats.data :as data]
   [reitit.frontend :as rf]
   [reitit.frontend.easy :as rfe]
   [reitit.coercion.spec :as rss]))


(defn hist-page []
  [hist/page])

(defn about-page []
  [about/page])

(defn stats-page []
  [stats/page])

(defonce match (r/atom nil))

(defn current-page []
  [:div
   [:ul
    [:li [:a {:href (rfe/href ::frontpage)} "Frontpage"]]
    [:li [:a {:href (rfe/href ::stats)} "Stats"]]
    [:li [:a {:href "https://framagit.org/adelpeyroux/veryultimatestats"} "Source Code"]]]
   (when @match
     (let [view (:view (:data @match))]
       [view @match]))])

(def routes
  [["/"
    {:name ::frontpage
     :view hist-page}]

   ["/about"
    {:name ::about
     :view about-page}]

   ["/stats"
    {:name ::stats
     :view stats-page}]])

(defn ^:export ^:dev/once init! []
  (data/retrieve-data)
  (rfe/start!
    (rf/router routes {:data {:coercion rss/coercion}})
    (fn [m] (reset! match m))
    ;; set to false to enable HistoryAPI
    {:use-fragment true})
  (d/render [current-page] (.getElementById js/document "app")))
