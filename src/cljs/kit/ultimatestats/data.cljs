(ns kit.ultimatestats.data
  (:require
   [ajax.core :refer [GET]]
   [reagent.core :as r]
   [clojure.string :as str]
   [kit.ultimatestats.utils :as utils]
   [kit.ultimatestats.time :as time]))

(def history (r/atom (sorted-map)))

(defn format-game [db-game]
  {:id (db-game :id)
   :playercount (db-game :playercount)
   :characters (str/split (db-game :characters) #",")
   :timestamp (time/get-date (db-game :timestamp))})

(defn set-games! [games]
  (reset! history (reduce #(assoc %1 (:id %2) %2) (sorted-map) games)))

(defn get-games []
  (vals @history))

(defn delete-game! [game]
  (swap! history dissoc (:id game))
  (js/console.log "Remove game " (:id game)))

(defn save-game! [game]
  (swap! history assoc (:id game) game)
  (js/console.log game))

(defn retrieve-data []
  (GET "/get-games" {:headers {"x-csrf-token" (.-value (.getElementById js/document "csrf-token"))}
                     :handler #(set-games! (:games (utils/json-to-dict %)))
                     :error-handler utils/error-handler
                     :reponse-format "application/json"}))
