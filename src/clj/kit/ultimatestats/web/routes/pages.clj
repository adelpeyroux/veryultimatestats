(ns kit.ultimatestats.web.routes.pages
  (:require
    [kit.ultimatestats.web.middleware.exception :as exception]
    [kit.ultimatestats.web.pages.layout :as layout]
    [integrant.core :as ig]
    [reitit.ring.coercion :as coercion]
    [ring.middleware.format :refer [wrap-restful-format]]
    [reitit.ring.middleware.muuntaja :as muuntaja]
    [reitit.ring.middleware.parameters :as parameters]
    [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
    [kit.ultimatestats.web.routes.utils :as utils]
    [kit.ultimatestats.web.controllers.ultimatestats :as ultimatestats]))

(defn wrap-page-defaults []
  (let [error-page (layout/error-page
                     {:status 403
                      :title "Invalid anti-forgery token"})]
    #(wrap-anti-forgery % {:error-response error-page})))

(defn home [{:keys [flash] :as request}]
  (let [{:keys [query-fn]} (utils/route-data request)]
    (layout/render request "home.html" {:errors (:errors flash)})))

;; Routes
(defn page-routes [_opts]
  [["/"          {:get home}]
   ["/get-games" {:get ultimatestats/get-games}]
   ["/save-game" {:post ultimatestats/save-game!}]
   ["/delete-game" {:post ultimatestats/delete-game!}]])

(defn route-data [opts]
  (merge
   opts
   {:middleware 
    [;; Default middleware for pages
     ;;(wrap-page-defaults)
     wrap-restful-format
     ;; query-params & form-params
     parameters/parameters-middleware
     ;; content-negotiation
     muuntaja/format-negotiate-middleware
     ;; encoding response body
     muuntaja/format-response-middleware
     ;; exception handling
     coercion/coerce-exceptions-middleware
     ;; decoding request body
     muuntaja/format-request-middleware
     ;; coercing response bodys
     coercion/coerce-response-middleware
     ;; coercing request parameters
     coercion/coerce-request-middleware
     ;; exception handling
     exception/wrap-exception]}))

(derive :reitit.routes/pages :reitit/routes)

(defmethod ig/init-key :reitit.routes/pages
  [_ {:keys [base-path]
      :or   {base-path ""}
      :as   opts}]
  (layout/init-selmer!)
  [base-path (route-data opts) (page-routes opts)])

