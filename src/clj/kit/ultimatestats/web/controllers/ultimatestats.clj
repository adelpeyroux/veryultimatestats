(ns kit.ultimatestats.web.controllers.ultimatestats
  (:require
   [kit.ultimatestats.web.pages.layout :as layout]
   [clojure.string :as str]
   [clojure.tools.logging :as log]
   [kit.ultimatestats.web.routes.utils :as utils]
   [ring.util.http-response :as http-response])
  (:import
    [java.util Date]))

(defn format-games
  [games]
  (log/debug "formating games" games)
  (map (fn [game] {:id (game :id)
                   :playercount (game :playercount)
                   :characters (str/split (game :characters) #",")
                   :timestamp (str (game :timestamp))})
       games))

(defn get-games
  [request]
  (let [{:keys [query-fn]} (utils/route-data request)]
    (layout/render request "games.json"
     {:games (format-games (query-fn :get-games {}))})))

(defn delete-game!
  [{{:strs [id]} :params :as request}]
  (log/debug "deleting game" id)
  (let [{:keys [query-fn]} (utils/route-data request)]
    (try
      (if (nil? id)
        (cond-> (http-response/bad-request)
          (nil? id)
          (assoc-in [:flash :errors :name] "id is required"))
        (do
          (query-fn :delete-game! {:id id})
          (http-response/ok)))
      (catch Exception e
        (log/error e "failed to delete game!")
        (-> (http-response/internal-server-error)
            (assoc :flash {:errors {:unknown (.getMessage e)}}))))))


(defn test-request
  [{:keys [params]}]
  (log/debug "TEST" params)
  (http-response/ok))


(defn save-game!
  [{{:strs [playercount characters]} :params :as request}]
  (log/debug "saving game" playercount characters)
  (let [{:keys [query-fn]} (utils/route-data request)]
    (try
      (if (or (nil? playercount) (empty? characters))
        (cond-> (http-response/bad-request)
          (nil? playercount)
          (assoc-in [:flash :errors :playercount] "playercount is required")
          (empty? characters)
          (assoc-in [:flash :errors :characters] "characters is required"))
        (let [result (query-fn :save-game! {:playercount playercount :characters characters})]
          (layout/render request "created-game.json" {:game (first (format-games result))})))
      (catch Exception e
        (log/error e "failed to save game!")
        (-> (http-response/internal-server-error)
            (assoc :flash {:errors {:unknown (.getMessage e)}}))))))
