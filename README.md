# VeryUltimateStats
![home page](images/home.png)
![stats page](images/stats.png)

## Description
Web app that allows to save Super smash bros game session. It also provides some primitive statistics

## Dependencies
In order to run this project you will need (at least):
* Java 17
* Npm and node
* Clojure 1.11

## Run localy 
Start a [REPL](#repls) in your editor or terminal of choice.

``` clojure
clj -M:dev:nrepl
```


Start the server with:

``` shell
(go)
```

To reload changes:

``` shell
(reset)
```

To compile ClojureScript code:

``` shell
npx shadow-cljs watch app
```

## Produce a .jar
To produce a full packaged jar file:

``` shell
clj -Sforce -T:build all
```

To run the server (see run.sh): 

``` shell
export JDBC_URL="jdbc:sqlite:ultimatestats_dev.db"
java -jar target/ultimatestats-standalone.jar
```

## License
Licenced under GPLv3.

