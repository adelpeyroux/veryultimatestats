-- :name save-game! :! :<!
-- :doc creates a new game entry
INSERT INTO game
(playercount, characters)
VALUES (:playercount, :characters)
RETURNING *

-- :name get-games :? :*
-- :doc selects all available games entries
SELECT * FROM game

-- :name delete-game! :! :n
-- :doc delete a given game entry
DELETE
FROM game
WHERE id=:id
